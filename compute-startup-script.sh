#!/usr/bin/env bash

export INSTANCE_TYPE="@INSTANCE_TYPE@" 
export CLUSTER_NAME="@CLUSTER_NAME@"
export DISABLE_HYPERTHREADING="@DISABLE_HYPERTHREADING@"
export PRINT_LOGS="True"
export SPINUP_TEST="False"

# Make sure other instances mount the /apps directory prior to executing any cluster services
mkdir -p /apps
echo "${CLUSTER_NAME}-controller:/apps	/apps	nfs	rw,hard,intr	0	0" >> /etc/fstab
echo "${CLUSTER_NAME}-controller:/home	/home	nfs	rw,hard,intr	0	0" >> /etc/fstab
echo "${CLUSTER_NAME}-controller:/etc/munge    /etc/munge     nfs      rw,hard,intr  0     0" >> /etc/fstab
echo "${CLUSTER_NAME}-controller:/opt    /opt     nfs      rw,hard,intr  0     0" >> /etc/fstab
echo "# ADDITIONAL MOUNTS #" >> /etc/fstab

# Needed to stop FAHClient so that users can run via CLI via batch script
service FAHClient stop
# ////////////////////////////////// #

/apps/cls/bin/cluster-services setup

if [ "$INSTANCE_TYPE" == "gpu-compute" ]; then
    nvidia-smi # Creates the device files
    systemctl enable nvidia-persistenced
    systemctl start nvidia-persistenced
    /sbin/modprobe nvidia-uvm
    mknod -m 666 /dev/nvidia-uvm c $(grep nvidia-uvm /proc/devices | awk '{print $1}') 0
fi

if [ "$DISABLE_HYPERTHREADING" == "True" ]; then
	sh /apps/hyperthreading/manage_hyperthreading.sh -d
fi

systemctl restart munge
systemctl restart slurmd

/apps/cls/bin/cluster-services system-checks
