#!/usr/bin/python3
import yaml

def get_cluster_configs():


    with open('./config.yaml', 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    with open('./fah-partitions.yaml', 'r') as stream:
        try:
            partitions = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    config['partitions'] = partitions

    return config

#END get_cluster_configs


config = get_cluster_configs()

config['compute_image'] = 'projects/fluid-cluster-ops/global/images/fluid-slurm-gcp-compute-centos-fah-v2-3-0'
pnames = []
for k in range(len(config['partitions'])):

    config['partitions'][k]['project'] = config['controller']['project']
    pnames.append(config['partitions'][k]['name'])
    for j in range(len(config['partitions'][k]['machines'])):
        region = '-'.join(config['partitions'][k]['machines'][j]['zone'].split('-')[0:2])
        vpc_subnet = 'https://www.googleapis.com/compute/v1/projects/{PROJECT}/regions/{REGION}/subnetworks/default'.format(PROJECT=config['controller']['project'],
                                                                                                                            REGION=region)
        config['partitions'][k]['machines'][j]['vpc_subnet'] = vpc_subnet


# Update slurm_accounts
filepath = './users.txt'
users = []
with open(filepath) as fp:
   line = fp.readline()
   cnt = 1
   while line:
       users.append(line.rstrip())    
       line = fp.readline()
       cnt += 1

config['slurm_accounts'] = [{'name':'fah',
                            'users':users,
                            'allowed_partitions':pnames}]


print( yaml.dump(config, default_flow_style=False))



