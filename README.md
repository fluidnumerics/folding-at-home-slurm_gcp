# Folding-at-home on Fluid-Slurm-GCP
Copyright 2020 Fluid Numerics LLC

Use this repository to setup fluid-slurm-gcp for use with folding at home


## Getting Started
1. Create a [fluid-slurm-gcp cluster](https://console.cloud.google.com/marketplace/details/fluid-cluster-ops/fluid-slurm-gcp)

2. Log in to your fluid-slurm-gcp controller instance 

3. Clone this repository
```
[user@controller] $ git clone https://bitbucket.org/fluidnumerics/folding-at-home-slurm_gcp.git
[user@controller] $ cd folding-at-home-slurm_gcp/
```

4. Go root.
```
[user@controller] $ sudo su
[root] #
```

5. Run setup.sh (this may take a couple minutes to update your partitions)
```
[root] # bash setup.sh
```

6. Log off the controller
```
[root] # exit
[user@controller] $ logout
```

7. Log in to your login node

6. Submit a job to start folding at home on fluid-slurm-gcp
```
[user@login] $ cd folding-at-home-slurm_gcp/
[user@login] $ sbatch --account=fah --partition=n1hcpu4 folding_at_home.slurm.batch
```

## Firewall rule setup
If you don't currently have firewall rules setup to allow tcp/udp traffic on ports 80 and 8080 for your default VPC network, you'll need to create firewall rules withe the following specifications
```
ports : 80, 8080
protocol : tcp, udp
network : default
```
Reach out to support@fluidnumerics.com for the `source_ranges` IP addresses.



## Running on GPUs
[**You may need to increase your GPU Quota on GCP across multiple regions before running on GPUs**](https://cloud.google.com/compute/quotas)
**GPU acceleration of FAH is currently not functional. Jobs will run but will not utilize GPUs. Experiment at your own risk**

You can run folding-at-home on v100 GPUs with
```
[user@login] $ sbatch --account=fah --partition=n1hcpu4-v100 --gres=gpu:1 folding_at_home.slurm.batch
```
or P100 GPUs with
```
[user@login] $ sbatch --account=fah --partition=n1hcpu4-p100 --gres=gpu:1 folding_at_home.slurm.batch
```

You can set time limits for your batch jobs (so that they don't just run forever) using the `--time` flag. For example to do one day of folding on a V100 GPU
```
[user@login] $ sbatch --account=fah --partition=n1hcpu4-v100 --gres=gpu:1 --time=1-00:00:00 folding_at_home.slurm.batch
```

## Tracking your contributions (optional)

If you want to track your contributions to folding at home, 

1. Get a passkey from FAH at https://apps.foldingathome.org/getpasskey

2. Set the FAH_PASSKEY and FAH_USERNAME environment variables on the login node
```
export FAH_USERNAME=YOUR-FAHUSERNAME
export FAH_PASSKEY=YOUR-FAHPASSKEY
```

3. Submit jobs

