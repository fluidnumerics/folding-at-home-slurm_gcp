#!/bin/bash

# Run this script on your controller as root #

ls -l /home | awk '{print $9}' > users.txt
sed -i '/^$/d' users.txt

/apps/cls/bin/cluster-services list all > config.yaml

python3 cluster_setup.py > fah_config.yaml

/apps/cls/bin/cluster-services update slurm_accounts --preview --config=fah_config.yaml
/apps/cls/bin/cluster-services update all --config=fah_config.yaml

rm -f /apps/slurm/scripts/compute-startup-script.sh
cp compute-startup-script.sh /apps/slurm/scripts/


